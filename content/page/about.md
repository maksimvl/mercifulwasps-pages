---
title: About Us
comments: false
---

**Team members:**

- Maksim Ljaussov
- Jaan Metlitski

**What we do in this project:**
- Tested Modern Application
- Selenium and CypressJs
- Testing Automation, CI/CD pipeline
