---
title: Tested Modern Application
subtitle: Java, Spring Boot, JUnit, REST Assured
tags: ["example", "code"]
---
**What are we do:**

1) API application that is calculate average top goals from Champions League competition using data from https://www.football-data.org API.

2) Unit tests

3) Integration tests

**Result of work:**

*Backend external api response* - [http://34.88.225.136:8080/api/scorers/top3](http://34.88.225.136:8080/api/scorers/top3)

*Backend api calculations* - https://mercifulwasps.tk/api/scorers/result

**Example request. Get top 3 scorers from Champions League:**
```
https://api.football-data.org/v2/competitions/CL/scorers?limit=3
```

Result:
```
{"count":3,"filters":{"limit":3},"competition":{"id":2001,"area":{"id":2077,"name":"Europe"},"name":"UEFA Champions League","code":"CL","plan":"TIER_ONE","lastUpdated":"2021-06-26T13:37:26Z"},"season":{"id":734,"startDate":"2021-06-26","endDate":"2022-05-22","currentMatchday":5,"winner":null},"scorers":[{"player":{"id":371,"name":"Robert Lewandowski","firstName":"Robert","lastName":null,"dateOfBirth":"1988-08-21","countryOfBirth":"Poland","nationality":"Poland","position":"Attacker","shirtNumber":null,"lastUpdated":"2021-03-05T13:51:30Z"},"team":{"id":5,"name":"FC Bayern München"},"numberOfGoals":8},{"player":{"id":6721,"name":"Sébastien Haller","firstName":"Sébastien","lastName":null,"dateOfBirth":"1994-06-22","countryOfBirth":"France","nationality":"Côte d’Ivoire","position":"Attacker","shirtNumber":null,"lastUpdated":"2020-11-12T02:23:45Z"},"team":{"id":678,"name":"AFC Ajax"},"numberOfGoals":7},{"player":{"id":81769,"name":"Adama Traoré","firstName":"Adama 'Malouda'","lastName":null,"dateOfBirth":"1995-06-05","countryOfBirth":"Mali","nationality":"Mali","position":"Midfielder","shirtNumber":null,"lastUpdated":"2019-09-06T05:10:56Z"},"team":{"id":1880,"name":"FC Sheriff Tiraspol"},"numberOfGoals":6}]}
```

Calculation result:
```
{"numberOfPlayersInTop":3,"totalGoals":21,"avgGoal":7.0}
```


**Git repository URL:**

https://gitlab.cs.ttu.ee/Maksim.Ljaussov/mercifulwasps-backend-api
