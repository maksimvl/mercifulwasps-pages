---
title: Selenium and CypressJS
subtitle: Java, Python
---
**What are we do:**

**1) Tested web application https://the-internet.herokuapp.com using Selenium and CypressJS frameworks**

**2) With Selenium we tested Add/Remove Elements & Status Codes examples**

4 tests for Add/Remove Elements:
- Can go to Add/Remove page
- Add one element 
- Add 10 elements
- Add 10 elements and delete 5 of them

4 tests for Status Codes:
- Status code 200 is presented
- Status code 301 is presented
- Status code 404 is presented
- Status code 500 is presented


**3) With CypressJS we tested JavaScript Alerts & Disappearing Elements**

4 tests for JavaScript Alerts:
- Can go to JavaScript Alerts page
- Alert is presented after click to alert button
- Status after accepting confirm window
- Text input in promt window

4 tests for Disappearing Elements:
- Can go to Disappearing Elements page
- Go to home page
- Go to about
- Go to portfolio

**4) Alternatively we tested Form Authentication using Python and Selenium.**

Total 4 tests:
- Can go to Form Authentication page
- Successful log in 
- Log out
- Not successful log in with wrong password

**Git repository URL:**

- Selenium repository https://gitlab.cs.ttu.ee/Maksim.Ljaussov/mercifulwasps-selenium
- CypressJS repository https://gitlab.cs.ttu.ee/Maksim.Ljaussov/mercifulwasps-cypress
- Python-Selenium repository https://gitlab.cs.ttu.ee/Maksim.Ljaussov/mercifulwasps-python-selenium