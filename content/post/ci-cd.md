---
title: CI/CD pipeline
subtitle: Testing Automation
---
**What are we do:**

Testing Automation, take back-end API to production and add CI/CD pipeline from project part "Tested Modern Application".

Production is set up using Google Cloud. Operating system is Ubuntu 20.04 LTS with 10 GB storage. Using GitLab CI/CD, we test, build, and publish our back end. Our domain is from freenom. Monitoring service - Zabbix. For installation guide please follow below.

**Component diagram**
![Crepe](https://i.imgur.com/FkKOspH.png)

**Result of work:**

*Backend external api response* - [http://34.88.225.136:8080/api/scorers/top3](http://34.88.225.136:8080/api/scorers/top3)

*Backend api calculations* - https://mercifulwasps.tk/api/scorers/result

*Zabbix monitor server* - [http://34.88.89.4](http://34.88.89.4)

**Set up instruction:**

**1. Creating new instance on Google cloud platform**

Server parameters:

- Operating system: Ubuntu 20.04 LTS
- Storage: SSD 10 GB
- Security: SSH keys


**2. Generating SSH keys**
```
ssh-keygen -t rsa -f ~/.ssh/mercifulwasps -C maksimlvest -b 2048  
```
Add public key to the instance

**3. Login to google cloud server:**
```
ssh -i [path to private key] [username]@34.88.225.136 
```
My connection:
```
ssh -i /Users/maxim/.ssh/mercifulwasps maksimlvest@34.88.225.136  
```


**4. Update and upgrade server**
- sudo apt-get update
- sudo apt-get upgrade


**5. Install java virtual machine**
- sudo apt-get install openjdk-11-jre openjdk-11-jdk


**6. Preparation of the backend**

gitlab-ci.yml is added to the backend project
```
stages:
  - build
  - test
  - deploy

before_script:
  - export GRADLE_USER_HOME=`pwd`/.gradle

build api:
  stage: build
  cache:
    paths:
      - .gradle/wrapper
      - .gradle/caches
  artifacts:
    paths:
      - build/libs
  script:
    - ./gradlew bootJar

test api:
  stage: test
  script:
    - ./gradlew check

deploy api:
  stage: deploy
  only:
    refs:
      - main
  script:
    - mkdir -p ~/api-deployment 
    - rm -rf ~/api-deployment/* 
    - cp -r build/libs/. ~/api-deployment 
    - sudo service mercifulwaspsapi restart
```


**7. Install gitlab runner**
- sudo curl -LJO https://gitlab-runner-downloads.s3.amazonaws.com/latest/deb/gitlab-runner_amd64.deb
- sudo dpkg -i gitlab-runner_amd64.deb


**8. Register a runner for the backend**
- sudo gitlab-runner register
- Add url and token from gitlab, executor is "shell"


**9. Sudo permissions to the gitlab-runner**
- sudo visudo

add the following to the bottom of the file
```
gitlab-runner ALL=(ALL) NOPASSWD: ALL /usr/sbin/service mercifulwasps api *
```


**10.  Add to gradle.build**
```
bootJar {
	archiveFileName = 'mercifulwasps-backend-api.jar'
}
```


**11. Create service from the backend**
- cd /etc/systemd/system/
- sudo touch mercifulwaspsapi.service

add next:
```
[Unit]
      Description=mercifulwasps service
      After=network.target
      
      [Service]
      Type=simple
      User=gitlab-runner
      WorkingDirectory=/home/gitlab-runner/api-deployment
      ExecStart=/usr/bin/java -jar mercifulwasps-backend-api.jar
      Restart=on-abort
      
      [Install]
      WantedBy=multi-user.target
```

Reload:

- sudo systemctl daemon-reload

Enable:

- sudo systemctl enable mercifulwaspsapi

Restart: 

- sudo service mercifulwaspsapi restart

Backend is visible at:

{IP address of the server}:8080/api/scorers


**13. Install nginx to the server:**
- sudo apt-get install nginx


**14. Modify a proxy setup for the backend in nginx:**
- cd /etc/nginx/sites-available
- sudo nano defaults

add
```
 location /api/scorers {  
         proxy_pass   http://localhost:8080;  
     } 
```
- sudo service nginx restart

Backend is visible at:

{IP address of the server}/api/scorers


**15. Domain registration on freenom**

Backend is visible at:

https://mercifulwasps.tk/api/scorers


**16. Https**
- sudo apt install certbot python3-certbot-nginx
- sudo nginx -t
- sudo systemctl reload nginx
- sudo certbot --nginx
- enter your domain name


**17. Adding Spring actuator:**

add dependencie:
```
 implementation 'org.springframework.boot:spring-boot-starter-actuator'
```

add to application.properties to enable all endpoints:
```
management.endpoints.web.exposure.include=*
management.endpoint.shutdown.enabled=true
```


**18. Adding Zabbix to monitor API:**
- Make a new server

- The server that will run the Zabbix needs Nginx, MySQL, and PHP installed. Follow Steps 1–3 of [https://www.digitalocean.com/community/tutorials/how-to-install-linux-nginx-mysql-php-lemp-stack-on-ubuntu-20-04]()

- Install and configure Zabbix: [https://www.digitalocean.com/community/tutorials/how-to-install-and-configure-zabbix-to-securely-monitor-remote-servers-on-ubuntu-20-04]()

- Add firewall rule on Google cloud API server to open 10050 port

- Zabbix runs in [http://34.88.89.4](http://34.88.89.4) with default username and password (Admin/zabbix)